import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:mood_diary/injection.dart';
import 'package:mood_diary/presentation/core/app_widget.dart';

Future<void> main() async {
  configureInjection(Environment.dev);

  await initializeDateFormatting('ru', 'RU');

  runApp(const AppWidget());
}
