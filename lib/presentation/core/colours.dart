import 'dart:ui';

abstract class Colours {
  static Color get background => const Color(0xFFFFFDFC);
  static Color get white => const Color(0xFFFFFFFF);
  static Color get black => const Color(0xFF000000);
  static Color get black1 => const Color(0xFF4C4C69);
  static Color get tangerine => const Color(0xFFFF8702);
  static Color get grey1 => const Color(0xFF98A1AA);
  static Color get grey2 => const Color(0xFFBCBCBF);
  static Color get grey3 => const Color(0xFFE8E8E8);
  static Color get grey4 => const Color(0xFFF2F2F2);
  static Color get grey5 => const Color(0xFFE1DDD8);
  static Color get red => const Color(0xFFFF3902);
}
