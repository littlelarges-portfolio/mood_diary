import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mood_diary/presentation/core/gen/fonts.gen.dart';

extension NumExtension on num {
  double get fr => r * .85;
}

abstract class TextStyles {
  static TextStyle nunito11Regular({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 11.fr,
        fontWeight: FontWeight.w400,
        color: color,
      );

  static TextStyle nunito12Medium({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 12.fr,
        fontWeight: FontWeight.w500,
        color: color,
      );

  static TextStyle nunito12SemiBold({
    Color? color,
    double? letterSpacing,
  }) =>
      TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 12.fr,
        fontWeight: FontWeight.w600,
        color: color,
        letterSpacing: letterSpacing,
      );

  static TextStyle nunito14Regular({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 14.fr,
        fontWeight: FontWeight.w400,
        color: color,
      );

  static TextStyle nunito16Bold({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 16.fr,
        fontWeight: FontWeight.w700,
        color: color,
      );

  static TextStyle nunito16ExtraBold({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 16.fr,
        fontWeight: FontWeight.w900,
        color: color,
      );

  static TextStyle nunito18SemiBold({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 18.fr,
        fontWeight: FontWeight.w600,
        color: color,
      );

  static TextStyle nunito18Medium({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 18.fr,
        fontWeight: FontWeight.w500,
        color: color,
      );

  static TextStyle nunito18Bold({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 18.fr,
        fontWeight: FontWeight.w700,
        color: color,
      );

  static TextStyle nunito20Regular({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 20.fr,
        fontWeight: FontWeight.w400,
        color: color,
      );

  static TextStyle nunito24Bold({Color? color}) => TextStyle(
        fontFamily: FontFamily.nunito,
        fontSize: 24.fr,
        fontWeight: FontWeight.w700,
        color: color,
      );
}
