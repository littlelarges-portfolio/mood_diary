// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:go_router/go_router.dart';
import 'package:mood_diary/presentation/calendar/widgets/custom_month_calendar.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/gen/assets.gen.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';

class CalendarMonthlyOverviewPage extends HookWidget {
  CalendarMonthlyOverviewPage({super.key});

  static const route = '/calenedar';

  double get _horizontalPadding => 20.r;

  double get _seperatedDistance => 30.r;

  final List<double> _heights = [];

  @override
  Widget build(BuildContext context) {
    final now = DateTime.now();

    final scrollController = useScrollController();

    void animateToIndex(int index) {
      if (_heights.isNotEmpty) {
        final animateTo = _heights.getRange(0, index).fold(
              0.toDouble(),
              (prev, value) => prev + value + _seperatedDistance,
            );

        scrollController.animateTo(
          animateTo,
          duration: const Duration(milliseconds: 300),
          curve: Curves.easeInOut,
        );
      }
    }

    return Scaffold(
      backgroundColor: Colours.background,
      appBar: AppBar(
        backgroundColor: Colours.background,
        leading: Row(
          children: [
            Gap(_horizontalPadding / 3),
            IconButton(
              padding: EdgeInsets.zero,
              icon: Assets.icons.cancel.svg(),
              onPressed: () {
                context.pop();
              },
            ),
          ],
        ),
        actions: [
          TextButton(
            onPressed: () {
              animateToIndex(now.month - 1);
            },
            child: Text(
              'Сегодня',
              style: TextStyles.nunito18SemiBold(color: Colours.grey2),
            ),
          ),
          Gap(_horizontalPadding),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(
          horizontal: _horizontalPadding,
        ),
        child: Column(
          children: [
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 16.r,
              ),
              height: 48.r,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'ПН',
                    style: TextStyles.nunito12SemiBold(letterSpacing: 3),
                  ),
                  Text(
                    'ВТ',
                    style: TextStyles.nunito12SemiBold(letterSpacing: 3),
                  ),
                  Text(
                    'СР',
                    style: TextStyles.nunito12SemiBold(letterSpacing: 3),
                  ),
                  Text(
                    'ЧТ',
                    style: TextStyles.nunito12SemiBold(letterSpacing: 3),
                  ),
                  Text(
                    'ПТ',
                    style: TextStyles.nunito12SemiBold(letterSpacing: 3),
                  ),
                  Text(
                    'СБ',
                    style: TextStyles.nunito12SemiBold(letterSpacing: 3),
                  ),
                  Text(
                    'ВС',
                    style: TextStyles.nunito12SemiBold(letterSpacing: 3),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Stack(
                children: [
                  ...List.generate(
                    12,
                    (index) => Opacity(
                      opacity: 0,
                      child: CustomMonthCalendar(
                        now: now,
                        month: index + 1,
                        onCalendarSizeObtained: (size) {
                          _heights.add(size.height);
                        },
                      ),
                    ),
                  ),
                  ListView.separated(
                    shrinkWrap: true,
                    controller: scrollController,
                    itemBuilder: (context, index) => CustomMonthCalendar(
                      now: now,
                      month: index + 1,
                    ),
                    separatorBuilder: (context, index) =>
                        Gap(_seperatedDistance),
                    itemCount: 12,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
