import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';

class DefaultCell extends StatelessWidget {
  const DefaultCell({
    required this.day,
    super.key,
  });

  final DateTime day;

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 38.r,
      height: 38.r,
      margin: EdgeInsets.zero,
      padding: EdgeInsets.zero,
      alignment: Alignment.center,
      decoration: BoxDecoration(
        color: Colours.white,
        shape: BoxShape.circle,
      ),
      child: Text(
        day.day.toString(),
        style: TextStyles.nunito18Medium(),
      ),
    );
  }
}
