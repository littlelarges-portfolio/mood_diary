import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:intl/intl.dart';
import 'package:mood_diary/presentation/calendar/core/helpers.dart';
import 'package:mood_diary/presentation/calendar/widgets/default_cell_widget.dart';
import 'package:mood_diary/presentation/calendar/widgets/today_cell_widget.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';
import 'package:table_calendar/table_calendar.dart';

class CustomMonthCalendar extends HookWidget {
  CustomMonthCalendar({
    required this.now,
    required this.month,
    this.onCalendarSizeObtained,
    super.key,
  }) : _getSizeGlobalKey = GlobalKey();

  final DateTime now;
  final int month;
  final void Function(Size)? onCalendarSizeObtained;

  final GlobalKey _getSizeGlobalKey;

  bool get _isCurrentMonth => month == now.month;

  void _getSize() {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      final context = _getSizeGlobalKey.currentContext;
      if (context != null) {
        final renderBox = context.findRenderObject();
        if (renderBox != null && renderBox is RenderBox) {
          final size = renderBox.size;
          onCalendarSizeObtained?.call(size);
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    useEffect(
      () {
        _getSize();
        return () {};
      },
      [],
    );

    return Column(
      key: _getSizeGlobalKey,
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        InkWell(
          borderRadius: BorderRadius.circular(10.r),
          onTap: () {
            // TODO(littlelarge): implement routing to the yearly calendar
          },
          child: Text(
            now.year.toString(),
            style: TextStyles.nunito16Bold(
              color: Colours.grey2,
            ),
          ),
        ),
        Gap(12.r),
        Text(
          month.toMonthString(),
          style: TextStyles.nunito24Bold(color: Colours.black1),
        ),
        Gap(17.r),
        TableCalendar(
          locale: 'ru_RU',
          headerVisible: false,
          daysOfWeekVisible: false,
          firstDay: DateTime.utc(now.year),
          lastDay: DateTime.utc(now.year, 12, 32),
          focusedDay: _isCurrentMonth ? now : DateTime(now.year, month),
          availableGestures: AvailableGestures.none,
          daysOfWeekStyle: DaysOfWeekStyle(
            weekdayStyle: TextStyles.nunito12Medium(),
            dowTextFormatter: (date, locale) {
              if (date.weekday == 1) {
                return DateFormat.E(locale).format(date).substring(0, 3);
              } else {
                return DateFormat.E(locale).format(date).substring(0, 2);
              }
            },
          ),
          calendarBuilders: CalendarBuilders<dynamic>(
            rangeStartBuilder: (context, day, focusedDay) {
              return Container();
            },
            rangeEndBuilder: (context, day, focusedDay) {
              return Container();
            },
            rangeHighlightBuilder: (context, day, focusedDay) {
              return Container();
            },
            disabledBuilder: (context, day, focusedDay) {
              return Container();
            },
            outsideBuilder: (context, day, focusedDay) {
              return Container();
            },
            defaultBuilder: (context, day, focusedDay) {
              return DefaultCell(
                day: day,
              );
            },
            todayBuilder: (context, day, focusedDay) {
              return _isCurrentMonth
                  ? TodayCell(day: day)
                  : DefaultCell(day: day);
            },
          ),
        ),
      ],
    );
  }
}
