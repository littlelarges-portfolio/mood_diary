import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';

class TodayCell extends StatelessWidget {
  const TodayCell({
    required this.day,
    super.key,
  });

  final DateTime day;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          width: 38.r,
          height: 38.r,
          margin: EdgeInsets.zero,
          padding: EdgeInsets.zero,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: Colours.tangerine.withOpacity(.25),
            shape: BoxShape.circle,
          ),
          child: Text(
            day.day.toString(),
            style: TextStyles.nunito18Medium(),
          ),
        ),
        Positioned(
          bottom: 3.r,
          right: 0,
          left: 0,
          child: Container(
            width: 5.r,
            height: 5.r,
            margin: EdgeInsets.zero,
            padding: EdgeInsets.zero,
            alignment: Alignment.center,
            decoration: BoxDecoration(
              color: Colours.tangerine,
              shape: BoxShape.circle,
            ),
            child: Text(
              day.day.toString(),
              style: TextStyles.nunito18Medium(),
            ),
          ),
        ),
      ],
    );
  }
}
