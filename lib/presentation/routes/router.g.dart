// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'router.dart';

// **************************************************************************
// GoRouterGenerator
// **************************************************************************

List<RouteBase> get $appRoutes => [
      $moodRoute,
      $calendarRoute,
    ];

RouteBase get $moodRoute => GoRouteData.$route(
      path: '/mood',
      factory: $MoodRouteExtension._fromState,
    );

extension $MoodRouteExtension on MoodRoute {
  static MoodRoute _fromState(GoRouterState state) => MoodRoute();

  String get location => GoRouteData.$location(
        '/mood',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}

RouteBase get $calendarRoute => GoRouteData.$route(
      path: '/calenedar',
      factory: $CalendarRouteExtension._fromState,
    );

extension $CalendarRouteExtension on CalendarRoute {
  static CalendarRoute _fromState(GoRouterState state) => CalendarRoute();

  String get location => GoRouteData.$location(
        '/calenedar',
      );

  void go(BuildContext context) => context.go(location);

  Future<T?> push<T>(BuildContext context) => context.push<T>(location);

  void pushReplacement(BuildContext context) =>
      context.pushReplacement(location);

  void replace(BuildContext context) => context.replace(location);
}
