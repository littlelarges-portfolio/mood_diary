import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:mood_diary/presentation/calendar/calendar_monthly_overview_page.dart';
import 'package:mood_diary/presentation/mood/mood_page.dart';

part 'router.g.dart';

final router = GoRouter(routes: $appRoutes, initialLocation: MoodPage.route);

@TypedGoRoute<MoodRoute>(
  path: MoodPage.route,
)
class MoodRoute extends GoRouteData {
  MoodRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) => const MoodPage();
}

@TypedGoRoute<CalendarRoute>(
  path: CalendarMonthlyOverviewPage.route,
)
class CalendarRoute extends GoRouteData {
  CalendarRoute();

  @override
  Widget build(BuildContext context, GoRouterState state) =>
      CalendarMonthlyOverviewPage();
}
