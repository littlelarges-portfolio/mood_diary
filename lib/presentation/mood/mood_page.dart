import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mood_diary/application/mood/mood_bloc.dart';
import 'package:mood_diary/injection.dart';
import 'package:mood_diary/presentation/mood/widgets/mood_form_widget.dart';

class MoodPage extends StatelessWidget {
  const MoodPage({super.key});

  static const route = '/mood';

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => getIt<MoodBloc>(),
      child: MoodForm(),
    );
  }
}
