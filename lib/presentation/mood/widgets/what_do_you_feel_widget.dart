import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:kt_dart/kt.dart';
import 'package:mood_diary/application/mood/mood_bloc.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';

class WhatDoYouFeel extends HookWidget {
  const WhatDoYouFeel({
    required this.moodFormHorizontalPadding,
    this.onFeelChanged,
    this.onFeelDetailChanged,
    super.key,
  });

  final double moodFormHorizontalPadding;
  final ValueChanged<int>? onFeelChanged;
  final ValueChanged<int>? onFeelDetailChanged;

  double get _feelCardElevation => .1.r;
  double get _feelDetailCardBorderRadius => 3.r;

  @override
  Widget build(BuildContext context) {
    final selected = useState(-1);
    final selectedDetail = useState(-1);

    return BlocBuilder<MoodBloc, MoodState>(
      buildWhen: (previous, current) => false,
      builder: (context, moodState) {
        return Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: [
            Padding(
              padding: EdgeInsets.symmetric(
                horizontal: moodFormHorizontalPadding,
              ),
              child: Text(
                'Что чувствуешь?',
                style: TextStyles.nunito16ExtraBold(
                  color: Colours.black1,
                ),
              ),
            ),
            Gap(20.r),
            SizedBox(
              height: 118.r,
              child: ListView.separated(
                padding: EdgeInsets.fromLTRB(
                  moodFormHorizontalPadding,
                  _feelCardElevation,
                  moodFormHorizontalPadding,
                  _feelCardElevation,
                ),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) => GestureDetector(
                  onTap: () {
                    if (selected.value != index) {
                      selected.value = index;
                    } else {
                      selected.value = -1;
                    }

                    selectedDetail.value = -1;

                    onFeelChanged?.call(selected.value);
                    onFeelDetailChanged?.call(selectedDetail.value);
                  },
                  child: Material(
                    elevation: _feelCardElevation,
                    borderRadius: BorderRadius.circular(76.r),
                    child: Container(
                      width: 83.r,
                      height: 118.r,
                      decoration: BoxDecoration(
                        color: Colours.white,
                        border: selected.value == index
                            ? Border.all(
                                color: Colours.tangerine,
                                width: 2.r,
                              )
                            : null,
                        borderRadius: BorderRadius.circular(76.r),
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            moodState.feelPatterns.get(index).name,
                            style: TextStyles.nunito11Regular(
                              color: Colours.black1,
                            ),
                          ),
                          Image.asset(
                            width: 53.r,
                            height: 50.r,
                            moodState.feelPatterns.get(index).iconPath,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                separatorBuilder: (context, index) => Gap(10.r),
                itemCount: moodState.feelPatterns.size,
              ),
            ),
            if (selected.value != -1)
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Gap(20.r),
                  Padding(
                    padding: EdgeInsets.symmetric(
                      horizontal: moodFormHorizontalPadding,
                    ),
                    child: Wrap(
                      spacing: 8.r,
                      runSpacing: 8.r,
                      children: moodState.feelPatterns[selected.value].details
                          .mapIndexed((index, item) {
                        return Material(
                          elevation: _feelCardElevation,
                          borderRadius: BorderRadius.circular(
                            _feelDetailCardBorderRadius,
                          ),
                          child: GestureDetector(
                            onTap: () {
                              selectedDetail.value = index;
                              
                              onFeelDetailChanged?.call(selectedDetail.value);
                            },
                            child: Container(
                              padding: EdgeInsets.symmetric(
                                horizontal: 8.r,
                                vertical: 3.r,
                              ),
                              decoration: BoxDecoration(
                                color: selectedDetail.value == index
                                    ? Colours.tangerine
                                    : Colours.white,
                                borderRadius: BorderRadius.circular(
                                  _feelDetailCardBorderRadius,
                                ),
                              ),
                              child: Text(
                                item,
                                textAlign: TextAlign.center,
                                style: TextStyles.nunito11Regular(
                                  color: selectedDetail.value == index
                                      ? Colours.white
                                      : Colours.black1,
                                ),
                              ),
                            ),
                          ),
                        );
                      }).asList(),
                    ),
                  ),
                ],
              ),
          ],
        );
      },
    );
  }
}
