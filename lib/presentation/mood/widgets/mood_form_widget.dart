import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:mood_diary/application/mood/mood_bloc.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/gen/assets.gen.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';
import 'package:mood_diary/presentation/mood/core/helpers.dart';
import 'package:mood_diary/presentation/mood/widgets/mood_slider_widget.dart';
import 'package:mood_diary/presentation/mood/widgets/notes_field_widget.dart';
import 'package:mood_diary/presentation/mood/widgets/successful_dialog.dart';
import 'package:mood_diary/presentation/mood/widgets/what_do_you_feel_widget.dart';
import 'package:mood_diary/presentation/routes/router.dart';

class MoodForm extends HookWidget {
  MoodForm({super.key});

  double get _horizontalPadding => 20.r;
  double get _verticalPadding => 4.r;
  double get _tabBarBorderRadius => 47.r;

  Color get _selectedTabTextColor => Colours.white;
  Color get _unselectedTabTextColor => Colours.grey2;

  final ValueNotifier<int> _feel = ValueNotifier(-1);
  final ValueNotifier<int> _feelDetail = ValueNotifier(-1);
  final ValueNotifier<String> _notes = ValueNotifier('');

  @override
  Widget build(BuildContext context) {
    final tabController = useTabController(initialLength: 2);

    return BlocBuilder<MoodBloc, MoodState>(
      builder: (context, moodState) {
        return Scaffold(
          backgroundColor: Colours.background,
          appBar: AppBar(
            backgroundColor: Colours.background,
            title: Text(
              DateTime.now().toFormattedMoodDay(),
              style: TextStyles.nunito18Bold(color: Colours.grey2),
            ),
            centerTitle: true,
            actions: [
              IconButton(
                onPressed: () {
                  CalendarRoute().push<CalendarRoute>(context);
                },
                icon: Assets.icons.calendar.svg(
                  height: 24.r,
                ),
              ),
              Gap(_horizontalPadding),
            ],
          ),
          body: Padding(
            padding: EdgeInsets.symmetric(
              vertical: _verticalPadding,
            ),
            child: Column(
              children: [
                Expanded(
                  child: Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(
                          horizontal: _horizontalPadding + 24.r,
                        ),
                        child: Container(
                          height: 30.r,
                          decoration: BoxDecoration(
                            color: Colours.grey4,
                            borderRadius: BorderRadius.circular(
                              _tabBarBorderRadius,
                            ),
                          ),
                          child: AnimatedBuilder(
                            animation: tabController.animation!,
                            builder: (context, child) {
                              final diaryTabColorTween = ColorTween(
                                begin: _selectedTabTextColor,
                                end: _unselectedTabTextColor,
                              ).animate(tabController.animation!);

                              final statisticTabColorTween = ColorTween(
                                begin: _unselectedTabTextColor,
                                end: _selectedTabTextColor,
                              ).animate(tabController.animation!);

                              return TabBar(
                                controller: tabController,
                                dividerColor: Colors.transparent,
                                overlayColor: const WidgetStatePropertyAll(
                                  Colors.transparent,
                                ),
                                indicatorColor: Colors.transparent,
                                labelColor: _selectedTabTextColor,
                                unselectedLabelColor: _unselectedTabTextColor,
                                indicatorSize: TabBarIndicatorSize.tab,
                                indicator: BoxDecoration(
                                  color: Colours.tangerine,
                                  borderRadius: BorderRadius.circular(
                                    _tabBarBorderRadius,
                                  ),
                                ),
                                tabs: [
                                  Tab(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Assets.icons.diary.svg(
                                          height: 12.r,
                                          colorFilter: ColorFilter.mode(
                                            diaryTabColorTween.value!,
                                            BlendMode.srcIn,
                                          ),
                                        ),
                                        Gap(6.r),
                                        Text(
                                          'Дневник настроения',
                                          style: TextStyles.nunito12Medium(),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Tab(
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Assets.icons.statistic.svg(
                                          height: 12.r,
                                          colorFilter: ColorFilter.mode(
                                            statisticTabColorTween.value!,
                                            BlendMode.srcIn,
                                          ),
                                        ),
                                        Gap(6.r),
                                        Text(
                                          'Статистика',
                                          style: TextStyles.nunito12Medium(),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              );
                            },
                          ),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: tabController,
                          children: [
                            SingleChildScrollView(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Gap(30.r),
                                  WhatDoYouFeel(
                                    moodFormHorizontalPadding:
                                        _horizontalPadding,
                                    onFeelChanged: (value) =>
                                        _feel.value = value,
                                    onFeelDetailChanged: (value) =>
                                        _feelDetail.value = value,
                                  ),
                                  Gap(36.r),
                                  MoodSlider(
                                    title: 'Уровень стресса',
                                    minLabel: 'Низкий',
                                    maxLabel: 'Высокий',
                                    moodFormHorizontalPadding:
                                        _horizontalPadding,
                                  ),
                                  Gap(36.r),
                                  MoodSlider(
                                    title: 'Самооценка',
                                    minLabel: 'Неуверенность',
                                    maxLabel: 'Уверенность',
                                    moodFormHorizontalPadding:
                                        _horizontalPadding,
                                  ),
                                  Gap(36.r),
                                  NotesField(
                                    moodFormHorizontalPadding:
                                        _horizontalPadding,
                                    onChanged: (value) => _notes.value = value,
                                  ),
                                  Gap(16.r),
                                  CustomButton(
                                    onTap: () {
                                      if (_notes.value.isNotEmpty &&
                                          _feel.value != -1 &&
                                          _feelDetail.value != -1) {
                                        showDialog<Widget>(
                                          context: context,
                                          builder: (context) =>
                                              const SuccessfulDialog(),
                                        );
                                      } else {
                                        ScaffoldMessenger.of(context)
                                            .showSnackBar(
                                          const SnackBar(
                                            content:
                                                Text('Не все поля заполнены'),
                                          ),
                                        );
                                      }
                                    },
                                  ),
                                  Gap(45.r),
                                ],
                              ),
                            ),
                            const Center(
                              child: Text(
                                'Статистика',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}

class CustomButton extends StatelessWidget {
  const CustomButton({
    required this.onTap,
    super.key,
  });

  final void Function() onTap;

  double get _borderRadius => 70.r;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          borderRadius: BorderRadius.circular(_borderRadius),
          onTap: onTap,
          child: Ink(
            padding: EdgeInsets.symmetric(
              horizontal: 117.r,
              vertical: 15.r,
            ),
            decoration: BoxDecoration(
              color: Colours.tangerine,
              borderRadius: BorderRadius.circular(_borderRadius),
            ),
            child: Text(
              'Сохранить',
              style: TextStyles.nunito20Regular(
                color: Colours.white,
              ),
            ),
          ),
        ),
      ),
    );
  }
}
