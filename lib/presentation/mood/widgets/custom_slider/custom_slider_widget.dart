import 'package:flutter/material.dart';
import 'package:flutter_hooks/flutter_hooks.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';
import 'package:mood_diary/presentation/mood/widgets/custom_slider/custom_thumb.dart';
import 'package:mood_diary/presentation/mood/widgets/custom_slider/custom_track_shape.dart';

class CustomSlider extends HookWidget {
  const CustomSlider({
    required this.moodFormHorizontalPadding,
    this.minLabel,
    this.maxLabel,
    super.key,
  });

  final double moodFormHorizontalPadding;
  final String? minLabel;
  final String? maxLabel;

  double get _sliderHorizontalPadding => 8.r;
  double get _sliderVerticalPadding => 16.r;
  double get _sliderCardBorderRadius => 13.r;

  @override
  Widget build(BuildContext context) {
    final stressLevelSliderValue = useState(.5);

    return Padding(
      padding: EdgeInsets.symmetric(
        horizontal: moodFormHorizontalPadding,
      ),
      child: Material(
        elevation: .1,
        borderRadius: BorderRadius.circular(
          _sliderCardBorderRadius,
        ),
        child: Container(
          padding: EdgeInsets.symmetric(
            horizontal: _sliderHorizontalPadding,
            vertical: _sliderVerticalPadding,
          ),
          decoration: BoxDecoration(
            color: Colours.white,
            borderRadius: BorderRadius.circular(
              _sliderCardBorderRadius,
            ),
          ),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: List.generate(
                  6,
                  (index) => Container(
                    width: 2.r,
                    height: 8.r,
                    color: Colours.grey5,
                  ),
                ),
              ),
              Gap(8.r),
              SliderTheme(
                data: SliderThemeData(
                  overlayShape: SliderComponentShape.noOverlay,
                  thumbColor: Colours.white,
                  trackShape: CustomTrackShape(),
                  thumbShape: const CustomThumb(),
                  activeTrackColor: Colours.tangerine,
                  inactiveTrackColor: Colours.grey5,
                  overlayColor: Colors.transparent,
                ),
                child: Slider(
                  onChanged: (value) {
                    stressLevelSliderValue.value = value;
                  },
                  value: stressLevelSliderValue.value,
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    minLabel ?? '',
                    style: TextStyles.nunito11Regular(
                      color: Colours.black1,
                    ),
                  ),
                  Text(
                    maxLabel ?? '',
                    style: TextStyles.nunito11Regular(
                      color: Colours.black1,
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
