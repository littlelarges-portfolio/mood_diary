import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';
import 'package:mood_diary/presentation/mood/widgets/custom_slider/custom_slider_widget.dart';

class MoodSlider extends StatelessWidget {
  const MoodSlider({
    required this.title,
    required this.moodFormHorizontalPadding,
    required this.minLabel,
    required this.maxLabel,
    super.key,
  });

  final double moodFormHorizontalPadding;
  final String title;
  final String minLabel;
  final String maxLabel;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: moodFormHorizontalPadding,
          ),
          child: Text(
            title,
            style: TextStyles.nunito16ExtraBold(
              color: Colours.black1,
            ),
          ),
        ),
        Gap(20.r),
        CustomSlider(
          moodFormHorizontalPadding: moodFormHorizontalPadding,
          minLabel: minLabel,
          maxLabel: maxLabel,
        ),
      ],
    );
  }
}
