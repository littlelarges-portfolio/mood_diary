import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:go_router/go_router.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';

class SuccessfulDialog extends StatelessWidget {
  const SuccessfulDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      child: SizedBox(
        height: 200.r,
        child: Column(
          children: [
            const Spacer(),
            const Text('Успешно!'),
            const Spacer(),
            TextButton(
              onPressed: () {
                context.pop();
              },
              child: Center(
                child: Text(
                  'Закрыть',
                  style: TextStyles.nunito16Bold(
                    color: Colours.red,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
