import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:gap/gap.dart';
import 'package:mood_diary/presentation/core/colours.dart';
import 'package:mood_diary/presentation/core/text_styles.dart';

class NotesField extends StatelessWidget {
  const NotesField({
    required this.moodFormHorizontalPadding,
    this.onChanged,
    super.key,
  });

  final double moodFormHorizontalPadding;
  final ValueChanged<String>? onChanged;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Padding(
          padding: EdgeInsets.symmetric(
            horizontal: moodFormHorizontalPadding,
          ),
          child: Text(
            'Заметки',
            style: TextStyles.nunito16ExtraBold(
              color: Colours.black1,
            ),
          ),
        ),
        Gap(20.r),
        Container(
          padding: EdgeInsets.symmetric(
            horizontal: moodFormHorizontalPadding,
          ),
          child: Material(
            elevation: .1,
            borderRadius: BorderRadius.circular(
              13.r,
            ),
            child: TextFormField(
              minLines: 3,
              maxLines: 10,
              maxLength: 250,
              onChanged: onChanged,
              decoration: InputDecoration(
                filled: true,
                fillColor: Colours.white,
                counterText: '',
                border: OutlineInputBorder(
                  borderSide: BorderSide.none,
                  borderRadius: BorderRadius.circular(13.r),
                ),
              ),
              style: TextStyles.nunito14Regular(
                color: Colours.black1,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
