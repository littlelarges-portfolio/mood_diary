import 'package:bloc/bloc.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:kt_dart/kt.dart';
import 'package:mood_diary/domain/mood/feel.dart';
import 'package:mood_diary/domain/mood/mood.dart';
import 'package:mood_diary/presentation/core/gen/assets.gen.dart';

part 'mood_event.dart';
part 'mood_state.dart';
part 'mood_bloc.freezed.dart';

@injectable
class MoodBloc extends Bloc<MoodEvent, MoodState> {
  MoodBloc() : super(MoodState.initial()) {
    on<MoodEvent>((event, emit) {
      event.map(
        added: (e) {},
        changed: (e) {},
      );
    });
  }
}
