part of 'mood_bloc.dart';

@freezed
class MoodState with _$MoodState {
  const factory MoodState({
    required KtList<Feel> feelPatterns,
    required KtList<Mood> moods,
  }) = _MoodState;

  factory MoodState.initial() => MoodState(
        feelPatterns: KtList.from([
          Feel(
            name: 'Радость',
            details: KtList.from([
              'Возбуждение',
              'Восторг',
              'Игривость',
              'Наслаждение',
              'Очарование',
              'Осознанность',
              'Смелость',
              'Удовольствие',
              'Чувственность',
              'Энергичность',
              'Экстравагантность',
            ]),
            iconPath: Assets.images.feels.joy.path,
          ),
          Feel(
            name: 'Страх',
            details: KtList.from([
              'Тревога',
              'Беспокойство',
              'Паника',
              'Нервозность',
              'Неуверенность',
              'Сомнение',
              'Напряженность',
              'Ужас',
              'Осторожность',
              'Взволнованность',
            ]),
            iconPath: Assets.images.feels.fear.path,
          ),
          Feel(
            name: 'Бешенство',
            details: KtList.from([
              'Гнев',
              'Раздражение',
              'Негодование',
              'Ярость',
              'Вспыльчивость',
              'Нетерпение',
              'Злость',
              'Агрессия',
              'Злоба',
              'Обида',
            ]),
            iconPath: Assets.images.feels.rage.path,
          ),
          Feel(
            name: 'Грусть',
            details: KtList.from([
              'Печаль',
              'Одиночество',
              'Разочарование',
              'Уныние',
              'Тоска',
              'Угнетенность',
              'Сожаление',
              'Потеря',
              'Меланхолия',
              'Отчаяние',
            ]),
            iconPath: Assets.images.feels.sadness.path,
          ),
          Feel(
            name: 'Спокойствие',
            details: KtList.from([
              'Умиротворение',
              'Расслабленность',
              'Гармония',
              'Довольство',
              'Уверенность',
              'Благодарность',
              'Баланс',
              'Невозмутимость',
              'Безмятежность',
              'Отрешенность',
            ]),
            iconPath: Assets.images.feels.calm.path,
          ),
          Feel(
            name: 'Сила',
            details: KtList.from([
              'Уверенность',
              'Решительность',
              'Целеустремленность',
              'Мужество',
              'Вдохновение',
              'Энергия',
              'Настойчивость',
              'Воля',
              'Оптимизм',
              'Восстановление',
            ]),
            iconPath: Assets.images.feels.strength.path,
          ),
        ]),
        moods: const KtList.empty(),
      );
}
