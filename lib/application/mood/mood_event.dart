part of 'mood_bloc.dart';

@freezed
class MoodEvent with _$MoodEvent {
  const factory MoodEvent.added({required Mood mood}) = _Added;
  const factory MoodEvent.changed({
    required int index,
    required Mood mood,
  }) = _Changed;
}
