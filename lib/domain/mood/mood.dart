import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mood_diary/domain/mood/feel.dart';

part 'mood.freezed.dart';

@freezed
abstract class Mood with _$Mood {
  const factory Mood({
    required Feel feel,
    required double stressLevel,
    required double selfEsteem,
    required String notes,
    required DateTime date,
  }) = _Mood;
}
