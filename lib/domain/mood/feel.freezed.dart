// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'feel.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Feel {
  String get name => throw _privateConstructorUsedError;
  KtList<String> get details => throw _privateConstructorUsedError;
  String get iconPath => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $FeelCopyWith<Feel> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $FeelCopyWith<$Res> {
  factory $FeelCopyWith(Feel value, $Res Function(Feel) then) =
      _$FeelCopyWithImpl<$Res, Feel>;
  @useResult
  $Res call({String name, KtList<String> details, String iconPath});
}

/// @nodoc
class _$FeelCopyWithImpl<$Res, $Val extends Feel>
    implements $FeelCopyWith<$Res> {
  _$FeelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? details = null,
    Object? iconPath = null,
  }) {
    return _then(_value.copyWith(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      details: null == details
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as KtList<String>,
      iconPath: null == iconPath
          ? _value.iconPath
          : iconPath // ignore: cast_nullable_to_non_nullable
              as String,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$FeelImplCopyWith<$Res> implements $FeelCopyWith<$Res> {
  factory _$$FeelImplCopyWith(
          _$FeelImpl value, $Res Function(_$FeelImpl) then) =
      __$$FeelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String name, KtList<String> details, String iconPath});
}

/// @nodoc
class __$$FeelImplCopyWithImpl<$Res>
    extends _$FeelCopyWithImpl<$Res, _$FeelImpl>
    implements _$$FeelImplCopyWith<$Res> {
  __$$FeelImplCopyWithImpl(_$FeelImpl _value, $Res Function(_$FeelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? name = null,
    Object? details = null,
    Object? iconPath = null,
  }) {
    return _then(_$FeelImpl(
      name: null == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String,
      details: null == details
          ? _value.details
          : details // ignore: cast_nullable_to_non_nullable
              as KtList<String>,
      iconPath: null == iconPath
          ? _value.iconPath
          : iconPath // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$FeelImpl implements _Feel {
  const _$FeelImpl(
      {required this.name, required this.details, required this.iconPath});

  @override
  final String name;
  @override
  final KtList<String> details;
  @override
  final String iconPath;

  @override
  String toString() {
    return 'Feel(name: $name, details: $details, iconPath: $iconPath)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$FeelImpl &&
            (identical(other.name, name) || other.name == name) &&
            (identical(other.details, details) || other.details == details) &&
            (identical(other.iconPath, iconPath) ||
                other.iconPath == iconPath));
  }

  @override
  int get hashCode => Object.hash(runtimeType, name, details, iconPath);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$FeelImplCopyWith<_$FeelImpl> get copyWith =>
      __$$FeelImplCopyWithImpl<_$FeelImpl>(this, _$identity);
}

abstract class _Feel implements Feel {
  const factory _Feel(
      {required final String name,
      required final KtList<String> details,
      required final String iconPath}) = _$FeelImpl;

  @override
  String get name;
  @override
  KtList<String> get details;
  @override
  String get iconPath;
  @override
  @JsonKey(ignore: true)
  _$$FeelImplCopyWith<_$FeelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
