// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'mood.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$Mood {
  Feel get feel => throw _privateConstructorUsedError;
  double get stressLevel => throw _privateConstructorUsedError;
  double get selfEsteem => throw _privateConstructorUsedError;
  String get notes => throw _privateConstructorUsedError;
  DateTime get date => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $MoodCopyWith<Mood> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $MoodCopyWith<$Res> {
  factory $MoodCopyWith(Mood value, $Res Function(Mood) then) =
      _$MoodCopyWithImpl<$Res, Mood>;
  @useResult
  $Res call(
      {Feel feel,
      double stressLevel,
      double selfEsteem,
      String notes,
      DateTime date});

  $FeelCopyWith<$Res> get feel;
}

/// @nodoc
class _$MoodCopyWithImpl<$Res, $Val extends Mood>
    implements $MoodCopyWith<$Res> {
  _$MoodCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? feel = null,
    Object? stressLevel = null,
    Object? selfEsteem = null,
    Object? notes = null,
    Object? date = null,
  }) {
    return _then(_value.copyWith(
      feel: null == feel
          ? _value.feel
          : feel // ignore: cast_nullable_to_non_nullable
              as Feel,
      stressLevel: null == stressLevel
          ? _value.stressLevel
          : stressLevel // ignore: cast_nullable_to_non_nullable
              as double,
      selfEsteem: null == selfEsteem
          ? _value.selfEsteem
          : selfEsteem // ignore: cast_nullable_to_non_nullable
              as double,
      notes: null == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $FeelCopyWith<$Res> get feel {
    return $FeelCopyWith<$Res>(_value.feel, (value) {
      return _then(_value.copyWith(feel: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$MoodImplCopyWith<$Res> implements $MoodCopyWith<$Res> {
  factory _$$MoodImplCopyWith(
          _$MoodImpl value, $Res Function(_$MoodImpl) then) =
      __$$MoodImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {Feel feel,
      double stressLevel,
      double selfEsteem,
      String notes,
      DateTime date});

  @override
  $FeelCopyWith<$Res> get feel;
}

/// @nodoc
class __$$MoodImplCopyWithImpl<$Res>
    extends _$MoodCopyWithImpl<$Res, _$MoodImpl>
    implements _$$MoodImplCopyWith<$Res> {
  __$$MoodImplCopyWithImpl(_$MoodImpl _value, $Res Function(_$MoodImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? feel = null,
    Object? stressLevel = null,
    Object? selfEsteem = null,
    Object? notes = null,
    Object? date = null,
  }) {
    return _then(_$MoodImpl(
      feel: null == feel
          ? _value.feel
          : feel // ignore: cast_nullable_to_non_nullable
              as Feel,
      stressLevel: null == stressLevel
          ? _value.stressLevel
          : stressLevel // ignore: cast_nullable_to_non_nullable
              as double,
      selfEsteem: null == selfEsteem
          ? _value.selfEsteem
          : selfEsteem // ignore: cast_nullable_to_non_nullable
              as double,
      notes: null == notes
          ? _value.notes
          : notes // ignore: cast_nullable_to_non_nullable
              as String,
      date: null == date
          ? _value.date
          : date // ignore: cast_nullable_to_non_nullable
              as DateTime,
    ));
  }
}

/// @nodoc

class _$MoodImpl implements _Mood {
  const _$MoodImpl(
      {required this.feel,
      required this.stressLevel,
      required this.selfEsteem,
      required this.notes,
      required this.date});

  @override
  final Feel feel;
  @override
  final double stressLevel;
  @override
  final double selfEsteem;
  @override
  final String notes;
  @override
  final DateTime date;

  @override
  String toString() {
    return 'Mood(feel: $feel, stressLevel: $stressLevel, selfEsteem: $selfEsteem, notes: $notes, date: $date)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$MoodImpl &&
            (identical(other.feel, feel) || other.feel == feel) &&
            (identical(other.stressLevel, stressLevel) ||
                other.stressLevel == stressLevel) &&
            (identical(other.selfEsteem, selfEsteem) ||
                other.selfEsteem == selfEsteem) &&
            (identical(other.notes, notes) || other.notes == notes) &&
            (identical(other.date, date) || other.date == date));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, feel, stressLevel, selfEsteem, notes, date);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$MoodImplCopyWith<_$MoodImpl> get copyWith =>
      __$$MoodImplCopyWithImpl<_$MoodImpl>(this, _$identity);
}

abstract class _Mood implements Mood {
  const factory _Mood(
      {required final Feel feel,
      required final double stressLevel,
      required final double selfEsteem,
      required final String notes,
      required final DateTime date}) = _$MoodImpl;

  @override
  Feel get feel;
  @override
  double get stressLevel;
  @override
  double get selfEsteem;
  @override
  String get notes;
  @override
  DateTime get date;
  @override
  @JsonKey(ignore: true)
  _$$MoodImplCopyWith<_$MoodImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
