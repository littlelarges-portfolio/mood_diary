import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:kt_dart/kt.dart';

part 'feel.freezed.dart';

@freezed
abstract class Feel with _$Feel {
  const factory Feel({
    required String name,
    required KtList<String> details,
    required String iconPath,
  }) = _Feel;
}
