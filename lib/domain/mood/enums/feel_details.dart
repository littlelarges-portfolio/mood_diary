enum JoyDetail {
  excitement,
  delight,
  playfulness,
  enjoyment,
  charm,
  awareness,
  courage,
  pleasure,
  sensuality,
  energy,
  extravagance,
}
