enum FeelType {
  joy,
  fear,
  rage,
  sadness,
  calm,
  strength,
}
